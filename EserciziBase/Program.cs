using System;
using System.Globalization;

namespace HelloWorld {
    class Program {
        static void Main(string[] args) {

            // commento
            int nSomma = 5;
            int b = 10;
            int c = 0;
            string d = "sdjdshasdh";
            long e = 9000000000000;

            if (nSomma < 5) {
                Console.WriteLine("a minore di 5");
            } else {
                Console.WriteLine("a maggiore o uguale a 5");
            }

            for (c = 0; c < 10; c++) {
                nSomma = nSomma + b;
            }
            Console.WriteLine("a vale {0}", nSomma);

            switch (nSomma) {
                case 1:
                    Console.WriteLine("a = 1");
                    break;
                case 2:
                    Console.WriteLine("a = 2");
                    break;
            }


            int[] arrInt = new[] { 3, 7, 4, 9, 21, 2 };
            int min = int.MaxValue;
            for (int k = 0; k < arrInt.Length; k++) {
                if (arrInt[k] < min) {
                    min = arrInt[k];
                }
            }
            Console.WriteLine("min = {0}", min);


            Console.WriteLine(ReverseStringDirect("ciao"));
            Console.WriteLine(IsStringPalindrome("ciao"));
            Console.WriteLine(IsStringPalindrome("anna"));
            Console.WriteLine(GetAge(System.DateTime.Parse("21/10/1972", new CultureInfo("it-IT"))));

            int numP = 0, currNum = 0;
            Console.Write("How many primes: ");
            int howManyPrimes = Convert.ToInt32(Console.ReadLine());
            while (numP < howManyPrimes) {
                if (IsPrime(currNum)) {
                    Console.WriteLine("{0} is prime", currNum);
                    currNum++;
                    numP++;
                } else {
                    currNum++;
                }
            }
            Console.ReadKey();
        }

        /* esercizi per la settimana */

        // calcolare la media aritmetica di un array di interi
        // stampare la somme dei primi N numeri pari (o dispari)
        // stampare i primi N numeri primi
        // invertire il contenuto di un array di interi (es. 1,2,3 deve diventare 3,2,1)
        // invertire una parola
        // determinare se una data parola è palindroma
        // calcolare l'età in anni data una determinata data di nascita






        public static string ReverseStringDirect(string s) {
            char[] array = new char[s.Length];
            int forward = 0;
            for (int i = s.Length - 1; i >= 0; i--) {
                array[forward++] = s[i];
            }
            return new string(array);
        }

        public static bool IsStringPalindrome(string myString) {
            int length = myString.Length;
            for (int i = 0; i < length / 2; i++) {
                if (myString[i] != myString[length - i - 1])
                    return false;
            }
            return true;
        }

        public static int GetAge(DateTime birthDate) {
            DateTime n = DateTime.Now; // To avoid a race condition around midnight
            int age = n.Year - birthDate.Year;

            if (n.Month < birthDate.Month || (n.Month == birthDate.Month && n.Day < birthDate.Day))
                age--;

            return age;
        }

        public static bool IsPrime(int num) {
            if (num == 0) return false;
            int ctr = 0;
            for (int i = 2; i <= num / 2; i++) {
                if (num % i == 0) {
                    ctr++;
                    break;
                }
            }
            if (ctr == 0 && num != 1)
                return true;
            else
                return false;
        }
    }

}




